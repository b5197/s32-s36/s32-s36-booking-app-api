	const jwt = require('jsonwebtoken');

//user defined string data that will be used to create our JSON web tokens.
//Used in the algorithm for encypting r data which makes it difficult to decode the information without the defined secret keyword
	const secret = 'CourseBookingAPI';

//JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access to certain parts of our app.

//Token Creation
//Analogy: Pack the gift and provide a lock with the secret code as the key
	module.exports.createAccessToken = (user) => {
		//check if we can receive the details of the user from our login
		console.log(user);
		//object to contain some details of our user
		//data within our payload
		const data = {
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		};
		//generate a JSON web token suing the jwt's sign method
		return jwt.sign(data, secret, {})
	};

//Token Verification
//Analogy: Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with.
	module.exports.verify = (req, res, next) => {
		//the token is retrieved from the request header
		console.log(req.headers.authorization)
		let token = req.headers.authorization
		//this statement will first check if the token variable contains undefined or a proper jwt. If it's undefined, we will check token's data type with typeOf, then send a message to the client.
		if (typeof token === 'undefined') {
			return res.send({auth: "Failed. No token"})
		} else {
			console.log(token);
			//bearer kjsdfhkjsadfhkajsdfh
			//slice(startingPosition, endPosition)
			token = token.slice(7, token.length)
			//validate the token using the verify method decrypting the token using the secret code
			jwt.verify(token, secret, function(err, decodedToken) {
				//err will contain the error from decoding your token. This will contain the reason why we will reject the token.
				// If verification of the token is successful, then jwt.verify will return the decoded token.
				if (err) {
					return res.send({
						auth: "Failed",
						message: err.message
					})
				} else {
					console.log(decodedToken) //contains data from our token
					req.user = decodedToken;
					//user property will be added to request object and will contain our decoded token.
					next();
				};
			});
		};
	};

//VERIFY an admin and will be used also as a middleware.
	module.exports.verifyAdmin = (req, res, next) => {
		if (req.user.isAdmin) {
			next();
		} else {
			return res.send({
				auth: "Failed",
				message: "Action Forbidden"
			})
		}
	};