//[SECTION] DEPENDENCIES AND MODULES
	const express = require('express');
	const controllers = require('../controllers/users');
	const auth = require('../auth');

//[SECTION] ROUTING COMPONENT
	const route = express.Router();

//[SECTION] ROUTES - POST
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body;
		controllers.register(userData).then(outcome => { res.send(outcome);
		});
	});

//[SECTION] ROUTES - USER AUTHENTICATION(LOGIN)
	route.post('/login', (req, res) => {
		controllers.loginUser(req.body).then(result => res.send(result));
	});

//[SECTION] ROUTES - GET
	route.get('/details', auth.verify, (req,res) => {
		controllers.getProfile(req.user.id).then(result => res.send(result));
	});

//[SECTION] ENROLL REGISTERED USER
	route.post('/enroll', auth.verify, controllers.enroll);

//[SECTION] GET ENROLLMENTS
	// route.get('/getEnrollments', auth.verify, controllers.getEnrollments);

//[SECTION] ROUTES - PUT


//[SECTION] ROUTES - DEL


//[SECTION] EXPOSE ROUTE SYSTEM
	module.exports = route;
