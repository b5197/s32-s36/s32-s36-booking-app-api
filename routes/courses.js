const express = require('express');
const route = express.Router();
const CourseController = require('../controllers/courses');
const auth = require('../auth');

//destructure tha actual function that we need to use
const {verify, verifyAdmin} = auth;

//Route for creating a course
	route.post('/create', verify, verifyAdmin, (req, res) => {
		CourseController.addCourse(req.body).then(result => res.send(result));
	});

//RETRIEVE ALL COURSES
	route.get('/all', (req, res) => {
		CourseController.getAllCourses().then(result => res.send(result));
	});

//Retrieve all ACTIVE courses
	route.get('/active', (req, res) => {
		CourseController.getAllActive().then(result => res.send(result));
	});

//RETRIEVE SPECIFIC COURSE
	route.get('/:courseId', (req, res) => {
		console.log(req.params.courseId)
		CourseController.getCourse(req.params.courseId).then(result => res.send(result));
	});

//[SECTION] UPDATE COURSE
	route.put('/:courseId', verify, verifyAdmin, (req, res) => {
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	});

//[SECTION] Archiving and Activating a course
	//Archiving a course
	route.put('/:courseId/archive', verify, verifyAdmin, (req, res) => {
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
	})

	//ACTIVATE a course
	route.put('/:courseId/activate', verify, verifyAdmin, (req, res) => {
		CourseController.activateCourse(req.params.courseId).then(result => res.send(result));
	})



module.exports = route;