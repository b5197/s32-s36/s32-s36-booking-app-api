//[SECTION] MODULES AND DEPENDENCIES
	const mongoose = require('mongoose');

//[SECTION] SCHEMA/BLUEPRINT
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, 'First name is Required']
		},
		lastName: {
			type: String,
			required: [true, 'Last name is Required']
		},
		email: {
			type: String,
			required: [true, 'Email is Required']
		},
		password: {
			type: String,
			required: [true, 'Password is Required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		mobileNum: {
			type: String,
			required: [true, 'Mobile number is required']
		},
		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'Subject ID is required']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'Enrolled'
				}
			}
		]
	});

//[SECTION] MODEL
	module.exports = mongoose.model('User', userSchema);