//[SECTION] MODULES AND DEPENDENCIES
	const mongoose = require('mongoose');

//[SECTION] SCHEMA/BLUEPRINT
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'is required']
		},
		description: {
			type: String,
			required: [true, 'is required']
		},
		price: {
			type: Number,
			required: [true, 'Course price is required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, 'Student ID is required']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
			}
		]
	});

//[SECTION] MODEL
	module.exports = mongoose.model('Course', courseSchema);