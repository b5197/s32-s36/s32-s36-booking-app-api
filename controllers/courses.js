//[SECTION] DEPENDENCIES AND MODULES
	const Course = require('../models/Course');

//[SECTION] CREATE COURSE
	/*
	Steps
	1. Create a new course object using the mongoose model and the information from the request body.
	2. Save the new Course to the database
	*/

	module.exports.addCourse = (reqBody) => {
		//create a variable "newCourse" and instantiate the name, desc, price
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		return newCourse.save().then((course, err ) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		}).catch(err => err)
	};

//[SECTION] GET COURSE
	//get all courses
	module.exports.getAllCourses = () => {
		return Course.find({}).then(result => {
			return result;
		});
	};

	//get all ACTIVE courses
	module.exports.getAllActive = () => {
		return Course.find({isActive: true}).then(result => {
			return result;
		}).catch(err => err)
	};

	//get SPECIFIC courses
	module.exports.getCourse = (reqParams) => {
		return Course.findById(reqParams).then(result => {
			return result;
		}).catch(err => err)
	};

//[SECTION] UPDATE COURSE
	//update course
	module.exports.updateCourse = (courseId, data) => {
		let updatedCourse = {
			name: data.name,
			description: data.description,
			price: data.price
		}
		return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	};

	//Archiving a course
	//1. update the status of "isActive" into "false" which will no longer be displayed in the client whenever all active courses are retrieved.
	module.exports.archiveCourse = (courseId) => {
		let updateActiveField = {
			isActive: false
		};

		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

	//Activating a course
	//1. update the status of "isActive" into "true" which will no longer be displayed in the client whenever all active courses are retrieved.
	module.exports.activateCourse = (courseId) => {
		let updateActiveField = {
			isActive: true
		};

		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}


