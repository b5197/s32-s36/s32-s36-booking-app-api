//[SECTION] DEPENDENCIES AND MODULES
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js')

//[SECTION] ENVIRONMENT VARIABLES SETUP
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] CREATE/POST
	module.exports.register = (userData) => {
		let fName = userData.firstName;		
		let lName = userData.lastName;
		let email = userData.email;
		let pass = userData.password;
		let mobNum = userData.mobileNum;
		//create new instance
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(pass, salt),
			mobileNum: mobNum
		});
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return {message: 'Failed to register account'};
			};
		});
	};

//[SECTION] USER AUTHENTICATION
	/*
	Steps:
	1. Check the database if the user email exists.
	2. Compare the password provided in the login form with password stored in the database.
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
	*/
	module.exports.loginUser = (data) => {
		//findOne method returns the first record in the collection that matches the search criteria
		return User.findOne({email:data.email}).then(result => {
			//if user does not exist
			if (result === null) {
				return false;
			//if user exists
			//'compareSync' method from the bcrypt to used in comparing the non encypted password from the login and the database password. It return 'true' or 'false' depending on the result
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
				//if the password match, return token
				if (isPasswordCorrect) {
					return {accessToken: auth.createAccessToken(result.toObject())}
				//pass didn't match
				} else {
					return false;
				};
			};
		});
	};


//[SECTION] RETRIEVE/GET
	module.exports.getProfile = data => {
		return User.findById(data).then(result => {
			result.password = '';
			return result;
		});
	};

//[SECTION] ENROLL REGISTERED USER
	/*
	Enrollment steps
	1. Look for the user by its ID
	2. Look for the course by its ID
	3. When both are successful, send a message to a client. Return true if successful, false if not.
	*/
	module.exports.enroll = async (req,res) => {
		console.log("Test Enroll Route");
		console.log(req.user.id);
		console.log(req.body.courseId);
		if(req.user.isAdmin) {
			return res.send({ message: "Action Forbidden"})
		}

	//get the user's ID to save the courseId inside the enrollments field
		let isUserUpdated = await User.findById(req.user.id).then(user => {
			let newEnrollment = {
				courseId: req.body.courseId
			}
			user.enrollments.push(newEnrollment)
			return user.save().then(user => true).catch(err => err.message);

			if (isUserUpdated !== true) {
				return res.send({message: isUserUpdated})
			}
		})
	//find courseId
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
			let enrollee = {
				userId: req.user.id
			}
			course.enrollees.push(enrollee);
			return course.save().then(course => true).catch(err => err.message)
			if (isCourseUpdated !== true) {
				return res.send({message: isCourseUpdated})
			}
		})
	//send msg to the client
		if (isUserUpdated && isCourseUpdated) {
			return res.send({message: "Enrolled Successfully"})
		}
	};


//GET ENROLLMENTS
	// module.exports.getEnrollments = (req, res) => {
	// 		User.findById(req.user.id)
	// 		.then(result => res.send(result.enrollments))
	// 		.catch(err => res.send(err))
	// }


//[SECTION] UPDATE/PUT


//[SECTION] DELETE